## Changelog 0.9.4 20240610


### added 

org:unitOf for the relation of an Organizational Unit to an Organization 



### changed

removed dcterms:subject and committee unit (the property is containd in the Organization Unit class) from committee matter 

### fixed

### To be addressed 

cv:Output instead of a schema:DigitalDocument

eli-dl:Decision  

*New class for representing the outcome the activity*

eli-dl:ActivityType a owl:Class;
  dc:identifier "ActivityType";
  rdfs:subClassOf crm:E55_Type, skos:Concept;
  rdfs:label "Type of an activity"@en;
  rdfs:comment "The type of an activity, e.g. \"Reading\", \"Committee meeting\", \"Economic and social committee meeting\", etc."@en 
  

  
  eli-dl:DecisionOutcome a owl:Class;
  dc:identifier "DecisionOutcome";
  rdfs:subClassOf crm:E55_Type, skos:Concept;
  rdfs:label "Decision outcome"@en;
  rdfs:comment "The possible outcomes of a decision. ELI-DL defines a simple list of possible values that can be extended by implementations"@en;
  owl:equivalentClass _:19b4974f821d43598180884296056517230 .

## Changelog 0.9.2 20240529


### added 

org:classification for type of committee 

dcterms:type for type of committee matter with constraint eli-dl:ActivityType

### changed

### fixed

### To be addressed 

cv:Output instead of a schema:DigitalDocument

eli-dl:Decision  

*New class for representing the outcome the activity*

eli-dl:ActivityType a owl:Class;
  dc:identifier "ActivityType";
  rdfs:subClassOf crm:E55_Type, skos:Concept;
  rdfs:label "Type of an activity"@en;
  rdfs:comment "The type of an activity, e.g. \"Reading\", \"Committee meeting\", \"Economic and social committee meeting\", etc."@en 
  

  
  eli-dl:DecisionOutcome a owl:Class;
  dc:identifier "DecisionOutcome";
  rdfs:subClassOf crm:E55_Type, skos:Concept;
  rdfs:label "Decision outcome"@en;
  rdfs:comment "The possible outcomes of a decision. ELI-DL defines a simple list of possible values that can be extended by implementations"@en;
  owl:equivalentClass _:19b4974f821d43598180884296056517230 .


## Changelog 0.9.0 20240419


### added 

prov:Activity instead of creating a new class 

prov:endedAtTime prov:startedAtTime replaces dcterms:dateSubmitted and dcterms:dateAccepted

prov:generated to link the activity to the document 

### To be addressed 

cv:Output instead of a schema:DigitalDocument

eli-dl:Decision  

*New class for representing the outcome the activity*

eli-dl:ActivityType a owl:Class;
  dc:identifier "ActivityType";
  rdfs:subClassOf crm:E55_Type, skos:Concept;
  rdfs:label "Type of an activity"@en;
  rdfs:comment "The type of an activity, e.g. \"Reading\", \"Committee meeting\", \"Economic and social committee meeting\", etc."@en 
  

  
  eli-dl:DecisionOutcome a owl:Class;
  dc:identifier "DecisionOutcome";
  rdfs:subClassOf crm:E55_Type, skos:Concept;
  rdfs:label "Decision outcome"@en;
  rdfs:comment "The possible outcomes of a decision. ELI-DL defines a simple list of possible values that can be extended by implementations"@en;
  owl:equivalentClass _:19b4974f821d43598180884296056517230 .
