rdforms_specs.init({
    language: document.targetLanguage,
    bundles: [
      ['../naemndsarenden0.9.8.json'],
    ],
    main: [
      'diariefordahandlingochnamndeprotokoll-20',
      'diariefordahandlingochnamndeprotokoll-43'
    ],
    supportive: [
      'diariefordahandlingochnamndeprotokoll-55',
      'diariefordahandlingochnamndeprotokoll-54',
//      'diariefordahandlingochnamndeprotokoll-52'
    ]
  });